# GIT COMMANDS #

**Check all local branches**
```
#!python
git branch
```

**Delete local branch**

```
#!python
git branch -d localBranch
```

**Or if branch not full merged**

```
#!python
git branch -D localBranch 
```

**For delete all merged branches**

```
#!python
git branch --merged | grep -v '*' | xargs git branch -D
```

**For merge origin branch to local branch**

```
#!python
git fetch 
git checkout -b localBranch 
git merge origin/originBranch
```

# TERMINAL COMMANDS FOR UNIX OS#

**Alias for delete all merged local branches**

```
#!python
alias git-clean="git branch  | grep -v '*' | grep -v 'develop' | xargs git branch -D  && git reset --hard && git clean -d -x -f"
```

**Alias mysql for mamp**

```
#!python
alias mysql='/Applications/MAMP/Library/bin/mysql'
```

**Alias php 7 for mamp**

```
#!python
alias php7='/Applications/MAMP/bin/php/php7.0.0/bin/php'
```

**Alias php 7 for mamp**

```
#!python
alias php5='/Applications/MAMP/bin/php/php5.6.10/bin/php'
```

**Alias for secondary skype**

```
#!python
alias skype2='sudo /Applications/Skype.app/Contents/MacOS/Skype /secondary'
```

**Change chown**

```
#!python
chown -Rc admin:psacln .
chown -c admin:psaserv .
```

**Change chown for w38**

```
#!python
chown -R w38_admin:psacln /var/www/vhosts/w38.epages.su/veneto.ua/
chown w38_admin:psaserv /var/www/vhosts/w38.epages.su/veneto.ua/
chown -R w38_admin:psacln /var/www/vhosts/w38.epages.su/b2b.veneto.ua/
chown w38_admin:psaserv /var/www/vhosts/w38.epages.su/b2b.veneto.ua/
```