<?php
/**
 * Created by PhpStorm.
 * User: k0mar
 * Date: 12/1/16
 * Time: 10:36
 */

namespace Epages\Helper;

use CIBlockElement,
    CUserFieldEnum;

class DataIblock
{
    /**
     * @param $permissibleId
     * @return array
     */
    public static function getMinPriceSections($permissibleId)
    {
        $prepareGetProducts = CIBlockElement::GetList(
            ['CATALOG_GROUP_1' => 'ASC'],
            [
                'ACTIVE' => 'Y',
                'SECTION_ID' => $permissibleId
            ],
            false,
            false,
            ['ID', 'CATALOG_GROUP_1', 'IBLOCK_SECTION_ID']
        );

        $arMinPrices = [];
        while ($getProducts = $prepareGetProducts->Fetch()) {
            $minPrice = explode('.', $getProducts['CATALOG_PRICE_1']);
            $arMinPrices[$getProducts['IBLOCK_SECTION_ID']] = [
                'MIN_PRICE' => $minPrice[0]
            ];
        }

        return $arMinPrices;
    }

    /**
     * @param $permissibleId
     * @return array
     */
    public static function getInfoFromFields($permissibleId)
    {
        $prepareFields = CUserFieldEnum::GetList(
            [],
            [
                'ID' => $permissibleId
            ]
        );

        $fieldsValues = [];
        while ($itemField = $prepareFields->Fetch()) {
            $fieldsValues[$itemField['ID']] = [
                'XML_ID' => $itemField['XML_ID'],
                'VALUE' => $itemField['VALUE']
            ];
        }

        return $fieldsValues;
    }
}