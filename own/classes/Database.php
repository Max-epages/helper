<?php
/**
 * Created by PhpStorm.
 * User: k0mar
 * Date: 11/10/16
 * Time: 16:41
 * 
 * For working with PDO
 */

namespace Epages\Helper;

use \PDO;

class Database extends PDO
{
    public function __construct($hostname, $user, $password, $dbname)
    {
        parent::__construct('mysql:host=' . $hostname . '; dbname=' . $dbname, $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
    }

    /**
     * select()
     * @param string $sql AN sql string
     * @param string $array Param to bind
     * @param cont $fetchMode
     * @return mixed
     * How its use : $this->db->select('SELECT * FROM users WHERE id = :id', array('id'=>'1'))
     */
    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_ASSOC)
    {
        $sth = $this->prepare($sql);

        foreach ($array as $key => $value) {
            $sth->bindValue(":$key", $value);
        }

        $sth->execute();
        return $sth->fetchAll($fetchMode);
    }

    /**
     * insert()
     * @param string $table This is a name of table in db
     * @param string $data This is associative array
     * How its use : $this->db->insert('nameTable', array('login'=>'test', 'password' => '123'));
     */
    public function insert($table, $data)
    {
        ksort($data);

        $fieldNames = implode('`, `', array_keys($data));
        $fieldValues = ':' . implode(', :', array_keys($data));

        $sth = $this->prepare("INSERT INTO $table (`$fieldNames`) VALUES($fieldValues)");
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        $sth->execute();
    }

    /**
     * update()
     * @param string $table This is a name of table in db
     * @param string $data This is associative array
     * @param string $where This is ID or another param for change needed column
     * How its use: $this->db->update('users',array('login' => 'test'), 'id = 1');
     */
    public function update($table, $data, $where)
    {
        ksort($data);

        $fieldDetails = NULL;
        foreach ($data as $key => $value) {
            $fieldDetails .= "`$key`=:$key,";
        }
        $fieldDetails = rtrim($fieldDetails, ',');

        $sth = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");
        foreach ($data as $key => $value) {
            $sth->bindValue(":$key", $value);
        }
        $sth->execute();
    }


    /**
     * delete()
     * @param string $table This is a name of table in db
     * @param string $where This is ID or another param for change needed column
     * @param int $limit How much count delete
     * @param return int
     * How its use:  $this->db->delete('users', 'id = 14');
     */
    public function delete($table, $where, $limit = 1)
    {
        return $this->exec("DELETE FROM $table WHERE $where LIMIT $limit");
    }
}